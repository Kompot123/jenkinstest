<?php


require 'app/ball.php';

class BallTest extends PHPUnit\Framework\TestCase
{
	public $ballInstance;
	
	public function setUp()
	{
		$this->ballInstance = new Ball();
	}
	
	public function testStealing(){
		$this->ballInstance->setBalls(100);
		
		$this->ballInstance->stealBall();
		
		$this->assertEquals(99,$this->ballInstance->getBalls());
	}
}