<?php

class Ball{
	
	private $ballCount;
	
	
	public function getBalls(){
		return $this->ballCount;
	}
	
	public function setBalls($number){
		$this->ballCount = $number;
	}
	
	public function stealBall(){
		$this->ballCount = $this->getBalls()-1;
	}
}